DEVICE_PACKAGE_OVERLAYS += \
    vendor/gapps/atv/overlay

PRODUCT_COPY_FILES += \
    vendor/gapps/atv/system/etc/permissions/com.google.android.pano.v1.xml:system/etc/permissions/com.google.android.pano.v1.xml \
    vendor/gapps/atv/system/etc/permissions/com.google.android.tv.installed.xml:system/etc/permissions/com.google.android.tv.installed.xml \
    vendor/gapps/atv/system/etc/permissions/com.google.widevine.software.drm.xml:system/etc/permissions/com.google.widevine.software.drm.xml \
    vendor/gapps/atv/system/etc/permissions/privapp-permissions-atv.xml:system/etc/permissions/privapp-permissions-atv.xml \
    vendor/gapps/atv/system/etc/default-permissions/default-permissions.xml:system/etc/default-permissions/default-permissions.xml \
    vendor/gapps/atv/system/etc/default-permissions/opengapps-permissions.xml:system/etc/default-permissions/opengapps-permissions.xml \
    vendor/gapps/atv/system/etc/permissions/privapp-permissions-google.xml:system/etc/permissions/privapp-permissions-google.xml

PRODUCT_COPY_FILES += \
    vendor/gapps/atv/system/etc/sysconfig/google.xml:system/etc/sysconfig/google.xml \
    vendor/gapps/atv/system/etc/sysconfig/google_atv.xml:system/etc/sysconfig/google_atv.xml \
    vendor/gapps/atv/system/etc/sysconfig/google_build.xml:system/etc/sysconfig/google_build.xml \
    vendor/gapps/atv/system/etc/sysconfig/google-hiddenapi-package-whitelist.xml:system/etc/sysconfig/google-hiddenapi-package-whitelist.xml

PRODUCT_PACKAGES += \
    com.google.widevine.software.drm \
    com.google.android.pano.v1

PRODUCT_PACKAGES += \
    com.google.android.atv.widget \
    com.google.android.backdrop \
    com.google.android.syncadapters.contacts \
    com.google.android.landscape \
    com.google.android.inputmethod.latin \
    com.google.android.play.games \
    com.google.android.youtube.tv

PRODUCT_PACKAGES += \
    com.google.android.tv.remote.service \
    com.google.android.pano.packageinstaller \
    com.google.android.configupdater \
    com.google.android.backuptransport \
    com.google.android.gsf \
    com.google.android.katniss \
    com.google.android.leanbacklauncher \
    com.google.android.tungsten.overscan \
    com.google.android.gms \
    com.android.vending \
    com.google.android.tv \
    com.google.android.tvlauncher \
    com.google.android.tvrecommendations

